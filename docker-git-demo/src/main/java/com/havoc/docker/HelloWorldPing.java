package com.havoc.docker;

/**
 * Hello world!
 *
 */
public class HelloWorldPing {
	
	public static void main(String args[]) throws Exception{
		for(int i = 0 ; i < 100; i++){
			System.out.println("HAVOC says Hello World Ping " + i );
			Thread.sleep(1000);
		}
	}
}
