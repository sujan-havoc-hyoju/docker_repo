Create a tomcat image from Dockerfile which does the following:
- pulls tomcat:8.5 image
- copies the war file to the webapps directory in tomcat
Build the image : docker build -t spring-mvc-image .
Run the image in a container : docker run -itd --name spring-mvc-container-1 -p 5555:8080 spring-mvc-image:latest
Execute shell for the container : docker exec -it <container-id> /bin/bash